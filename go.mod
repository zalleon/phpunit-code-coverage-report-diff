module gitlab.com/zalleon/phpunit-code-coverage-report-diff

go 1.12

require (
	github.com/mattn/go-runewidth v0.0.8 // indirect
	github.com/tatsushid/go-prettytable v0.0.0-20141013043238-ed2d14c29939
	golang.org/x/net v0.0.0-20200114155413-6afb5195e5aa
)
