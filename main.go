package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"regexp"
	"strings"

	"golang.org/x/net/html"

	"github.com/tatsushid/go-prettytable"
)

var debug = false
var regName = regexp.MustCompile(`doc>\.0>html\.1>body\.2>div\.3>table\.1>tbody\.3>tr\.([0-9]+)>td\.1$`)
var regNameA = regexp.MustCompile(`doc>\.0>html\.1>body\.2>div\.3>table\.1>tbody\.3>tr\.([0-9]+)>td\.1>a\.2`)
var regValue = regexp.MustCompile(`doc>\.0>html\.1>body\.2>div\.3>table\.1>tbody\.3>tr\.([0-9]+)>td\.5>div\.0`)
var regLines = regexp.MustCompile(`doc>\.0>html\.1>body\.2>div\.3>table\.1>tbody\.3>tr\.([0-9]+)>td\.7>div\.0`)

type Row struct {
	File     string
	Coverage string
	Lines    string
}

type RowCollection map[string]Row

func main() {
	base := flag.String("base", "", "Path to base coverage report")
	target := flag.String("target", "", "Path to target coverage report")
	verbose := flag.Bool("verbose", false, "Full log")

	flag.Parse()

	if *base == "" {
		fmt.Println("undefined base path -base")
		return
	}

	if *target == "" {
		fmt.Println("undefined target path -target")
		return
	}

	tbl, err := prettytable.NewTable([]prettytable.Column{
		{Header: "Path"},
		{Header: "Base coverage"},
		{Header: "Target coverage"},
		{Header: "Base lines"},
		{Header: "Target lines"},
	}...)
	check(err)
	tbl.Separator = " | "

	compare(*base, *target, "index.html", 0, tbl, *verbose)

	_, err = tbl.Print()
	check(err)
}

func compare(p1, p2, f string, level int, table *prettytable.Table, verbose bool) {
	t1 := parseReportFile(p1, f)
	t2 := parseReportFile(p2, f)

	t := make(map[string]bool)

	for file, _ := range t1 {
		t[file] = true
	}
	for file, _ := range t2 {
		if _, ok := t[file]; !ok {
			t[file] = true
		}
	}

	for file, _ := range t {
		r1, c1Exists := t1[file]
		r2, c2Exists := t2[file]

		if c1Exists && c2Exists {
			if r1.Coverage != r2.Coverage || (level > 0 && r1.Lines != r2.Lines) {
				if verbose {
					_ = table.AddRow(p1+file, r1.Coverage, r2.Coverage, r1.Lines, r2.Lines)
				} else if strings.Contains(file, ".php") {
					_ = table.AddRow(p1+file, r1.Coverage, r2.Coverage, r1.Lines, r2.Lines)
				}
				if !strings.Contains(file, ".php") && !strings.Contains(file, "Total") {
					compare(p1+file+"/", p2+file+"/", f, level+1, table, verbose)
				}
			}
		} else if c1Exists == true && c2Exists == false {
			_ = table.AddRow(p1+file, r1.Coverage, "undefined", r1.Lines, "undefined")
		} else {
			_ = table.AddRow(p1+file, "undefined", r2.Coverage, "undefined", r2.Lines)
		}
	}
}

func parseReportFile(path, file string) map[string]Row {

	reportFile, err := os.Open(path + file)
	check(err)

	doc, err := html.Parse(bufio.NewReader(reportFile))
	check(err)

	var f func(*html.Node, string, int, *RowCollection)
	f = func(n *html.Node, path string, idx int, table *RowCollection) {
		switch n.Type {
		case html.ElementNode:
			//if n.Data == "a" {
			//	for _, a := range n.Attr {
			//		if a.Key == "href" {
			//			fmt.Println(fmt.Sprintf("Path: %s Node: %s Attr: href='%s'", path, n.Data, a.Val))
			//			break
			//		}
			//	}
			//} else {
			//	fmt.Println(fmt.Sprintf("Path: %s Node: %s", path, n.Data))
			//}
		case html.TextNode:
			text := strings.ReplaceAll(n.Data, "\n", "")
			text = strings.ReplaceAll(text, " ", "")
			text = strings.ReplaceAll(text, "%", "")

			//fmt.Println(fmt.Sprintf("%s->%s", path, text))

			if regName.MatchString(path) && text != "" {
				rowIdx := getRowIdx(regName, path)

				setFile(table, rowIdx, text)

				if debug {
					fmt.Println(fmt.Sprintf("Path: %s Name: %s[%s]", path, text, rowIdx))
				}
			}

			if regNameA.MatchString(path) && text != "" {
				rowIdx := getRowIdx(regNameA, path)

				setFile(table, rowIdx, text)

				if debug {
					fmt.Println(fmt.Sprintf("Path: %s Name: %s[%s]", path, text, rowIdx))
				}
			}

			if regValue.MatchString(path) {
				rowIdx := getRowIdx(regValue, path)

				setCoverage(table, rowIdx, text)

				if debug {
					fmt.Println(fmt.Sprintf("Path: %s Value: %s[%s]", path, text, rowIdx))
				}
			}

			if regLines.MatchString(path) {
				rowIdx := getRowIdx(regLines, path)

				setLines(table, rowIdx, text)

				if debug {
					fmt.Println(fmt.Sprintf("Path: %s Lines: %s[%s]", path, text, rowIdx))
				}
			}
		}

		i := 0
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			f(c, fmt.Sprintf("%s>%s.%d", path, n.Data, idx), i, table)
			i++
		}
	}

	table := make(RowCollection)
	f(doc, "doc", 0, &table)

	err = reportFile.Close()
	check(err)

	result := make(map[string]Row)
	for _, row := range table {
		result[row.File] = row
	}

	return result
}

func getRowIdx(r *regexp.Regexp, path string) string {
	matches := r.FindSubmatch([]byte(path))
	rowIdx := ""
	if matches != nil {
		rowIdx = string(matches[1])
	}
	return rowIdx
}

func setFile(table *RowCollection, rowIdx string, text string) {
	row := (*table)[rowIdx]
	row.File = text
	(*table)[rowIdx] = row
}

func setCoverage(table *RowCollection, rowIdx string, text string) {
	row := (*table)[rowIdx]
	row.Coverage = text
	(*table)[rowIdx] = row
}

func setLines(table *RowCollection, rowIdx string, text string) {
	row := (*table)[rowIdx]
	row.Lines = text
	(*table)[rowIdx] = row
}

func check(e error) {
	if e != nil {
		log.Fatal(e)
	}
}
