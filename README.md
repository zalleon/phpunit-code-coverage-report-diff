# PHPUnit Code Coverage Report Diff
Compare phpunit code coverage reports

## Params
-base=/Path/To/Base/Report/
-target=/Path/To/Target/Report/
-verbose

## Docker RUN
`$ docker build -t phpunit-code-coverage-report-diff-image .`

`$ docker run -v /path/to/Artifacts:/Artifacts phpunit-code-coverage-report-diff-image -base=/Artifacts/artifacts-base/test_coverage_report/ -target=/Artifacts/artifacts-target/test_coverage_report/`