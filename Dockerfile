############################
# STEP 1 build executable binary
############################
FROM golang:1.12.15-alpine AS builder

# Install git + SSL ca certificates.
# Git is required for fetching the dependencies.
# Ca-certificates is required to call HTTPS endpoints.
RUN apk update && apk add --no-cache git

ENV GO111MODULE=on

COPY . /phpunit-code-coverage-report-diff

WORKDIR /phpunit-code-coverage-report-diff

# Fetch dependencies.
# Using go mod with go 1.11
RUN go mod download
RUN go mod verify

# Build the binary
RUN GOOS=linux GOARCH=amd64 GO111MODULE=on go build -ldflags="-w -s" -o /phpunit-code-coverage-report-diff/bin/application

############################
# STEP 2 build a small image
############################
FROM scratch

# Copy our static executable
COPY --from=builder /phpunit-code-coverage-report-diff/bin/application /phpunit-code-coverage-report-diff/bin/application

# Run the hello binary.
ENTRYPOINT ["/phpunit-code-coverage-report-diff/bin/application"]